defmodule Brc do
  def read_file(path \\ "weather_stations.csv") do
    File.stream!(path, [], :line)
    |> Stream.map(&String.split(&1, ";"))
  end

  def process_values(values) do
    values
    |> Stream.map(fn [city, temp] ->
      temp = String.slice(temp, 0, String.length(temp) - 1)
      {temp, _rest} = Float.parse(temp)
      {city, temp}
    end)
    |> Enum.reduce(%{}, fn {city, temp}, acc ->
      city_vals = Map.get(acc, city, %{sum: 0, count: 0, min: 100.0, max: -100.0})

      city_vals = %{
        sum: temp + city_vals.sum,
        count: city_vals.count + 1,
        min: min(temp, city_vals.min),
        max: max(temp, city_vals.max)
      }

      acc
      |> Map.put(city, city_vals)
    end)
    |> Stream.map(fn {city, city_vals} -> {city, %{min: city_vals.min, max: city_vals.max, avg: city_vals.sum / city_vals.count}} end)
  end

  def process_parallel(values) do
    nprocs = :erlang.system_info(:logical_processors)
    collector = self()
    IO.puts("number of processes: #{nprocs}")
    values
    |> Enum.sort_by(&(Enum.fetch!(&1, 0)))
    |> Enum.group_by(&(Enum.fetch!(&1, 0)))
    |> Stream.chunk_every(nprocs)
    |> Enum.map(fn chunk ->
      spawn(fn -> 
        chunk 
        |> Enum.map(fn {_city, vals} -> send(collector, process_values(vals)) 
        end) 
      end)
    end)

    write_results(File.open!("data/measurements.txt", [:write]), nprocs)
  end

  defp write_results(file, nprocs, nreceived \\ 0) do
    receive do 
      chunk -> if nreceived == nprocs do
        :ok
        else
          IO.write(file, Enum.reduce(chunk, "", fn {city, city_vals}, acc -> acc <> "#{city} avg=#{city_vals.avg} min=#{city_vals.min} max=#{city_vals.max}" end))
          write_results(file, nprocs, nreceived + 1)
      end
    end
  end


  def main(file \\ nil) do
    file = file || System.fetch_env!("DATA_PATH")
    {uSec, :ok} =
      :timer.tc(fn ->
        read_file(file) 
        |> process_parallel()
        :ok
      end)

    IO.puts("It took #{uSec / 1000_000} seconds")
  end
end
